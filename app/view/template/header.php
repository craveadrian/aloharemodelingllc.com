<?php $this->suspensionRedirect($view); ?>
<!DOCTYPE html>
<html lang="en" <?php $this->helpers->htmlClasses(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />

	<?php $this->helpers->seo($view); ?>
	<link rel="icon" href="public/images/favicon.png" type="image/x-icon">
	<!-- <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"> -->
	<link href="<?php echo URL; ?>public/styles/style.css" rel="stylesheet">
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
	<link rel="stylesheet" href="<?php echo URL; ?>public/fancybox/source/jquery.fancybox.css" media="screen" />
	<?php $this->helpers->analytics(); ?>
</head>

<body <?php $this->helpers->bodyClasses($view); ?>>
<?php $this->checkSuspensionHeader(); ?>
	<header>
		<div id="header">
			<div class="row">
				<nav>
					<a href="#" id="pull"></a>
					<ul>
						<li <?php $this->helpers->isActiveMenu("home"); ?>><a href="<?php echo URL ?>" title="HOME">HOME</a></li>
						<li <?php $this->helpers->isActiveMenu("about"); ?>><a href="<?php echo URL ?>about#content" title="ABOUT US">ABOUT US</a></li>
						<li <?php $this->helpers->isActiveMenu("services"); ?>><a href="<?php echo URL ?>services#content" title="SERVICES">SERVICES</a></li>
						<li <?php $this->helpers->isActiveMenu("gallery"); ?>><a href="<?php echo URL ?>gallery#content" title="GALLERY">GALLERY</a></li>
						<!-- <li <?php //$this->helpers->isActiveMenu("reviews"); ?>><a href="<?php //echo URL ?>reviews#content" title="REVIEWS">REVIEWS</a></li> -->
						<li <?php $this->helpers->isActiveMenu("contact"); ?>><a href="<?php echo URL ?>contact#content" title="CONTACT US">CONTACT US</a></li>
					</ul>
				</nav>
				<p>CALL US AT: <span><?php $this->info(["phone","tel"]);?></span></p>
			</div>
		</div>
	</header>

	<div id="banner">
		<div class="row">
			<a href="home"><img src="./public/images/common/mainLogo-hd.png" alt="Main Logo Header" class="mainLogo"></a>
			<h2>100% CUSTOMER SATISFACTION<span>We’re proud to be the preferred home renovations provider in Honolulu, HI</span></h2>
			<a href="contact#content" class="bttn">FREE ESTIMATE</a>
		</div>
	</div>
