<div id="content">
	<div class="row">
		<h1>Our Services</h1>
		<p class="services-paragraph">We are a construction and renovation company providing all required phases of your next project. Each project is unique, as well as each client, so the services we provide greatly depend on the scope of work</p>
		<div class="service-images">
			<img src="public/images/content/service/1.jpg" alt="">
			<img src="public/images/content/service/2.jpg" alt="">
			<img src="public/images/content/service/3.jpg" alt="">
			<img src="public/images/content/service/4.jpg" alt="">
			<img src="public/images/content/service/5.jpg" alt="">
			<img src="public/images/content/service/6.jpg" alt="">
		</div>
	</div>
</div>
