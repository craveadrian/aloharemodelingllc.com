<div id="content">
	<div class="row">
		<div class="leftCont">
			<p>welcome to</p>
			<h2>ALOHA REMODELING & <span>HANDIMAN SERVICES</span></h2>
			<div class="desc">
				<p>Aloha Remodeling & Handiman Services is that someone who can do the hands-on work around the house quickly and effectivn hire professionals. With a general contractor, you can rely on us to build and renovate anything you need.</p>
				<p>At Aloha Remodeling & Handiman Services, LLC, we’re proud to be the preferred general contractor’s provider in Honolulu, HI. Get in touch to learn more about what we can do for you.</p>
			</div>
		</div>
		<div class="image-Cont">
			<img src="public/images/content/content-img2.png" alt="Content Image 1">
			<img src="public/images/content/content-img3.png" alt="Content Image 2">
		</div>
		<img src="public/images/content/content-img1.png" alt="Content Main Image">
	</div>
</div>
<div id="services">
	<div class="row">
		<h2>OUR SERVICES</h2>
		<div class="img-Cont">
			<div class="cols col-1">
				<div class="imgph">
					<img src="public/images/content/services-img1.png" alt="Service Image 1">
					<p>NEW HOME CUSTRUCTION</p>
				</div>
				<div class="imgph">
					<img src="public/images/content/services-img2.png" alt="Service Image 2">
					<p>BATHROOM</p>
				</div>
			</div>
			<div class="cols col-2">
				<div class="imgph">
					<img src="public/images/content/services-img3.png" alt="Service Image 3">
					<p>CARPENTRY</p>
				</div>
				<div class="imgph">
					<img src="public/images/content/services-img4.png" alt="Service Image 4">
					<p>HOME ADDITIONS</p>
				</div>
			</div>
			<div class="cols col-3">
				<div class="imgph">
					<img src="public/images/content/services-img5.png" alt="Service Image 5">
					<p>DECKING</p>
				</div>
				<div class="imgph">
					<img src="public/images/content/services-img6.png" alt="Service Image 6">
					<p>HANDYMAN</p>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="info">
	<div class="row">
		<div class="info-Cont">
			<h2>WHY CHOOSE US?</h2>
			<div class="list">
				<ul>
					<li>High Quality Work</li>
					<li>Punctual. Professional Staff</li>
					<li>Prompt Project Completion</li>
					<li>Over 40 Years Of Experience</li>
				</ul>
				<ul>
					<li>Fully Insured</li>
					<li>Project Planning Assistance</li>
					<li>Great Customer Service</li>
					<li>Locally Owned & Operated</li>
				</ul>
			</div>
			<a href="https://www.homeadvisor.com/" target="_blank"><img src="public/images/content/home-advisor-logo.png" alt="" class="hadvisor"></a>
		</div>
	</div>
</div>
<div id="contact">
	<div class="row">
		<h2>CONTACT US</h2>
		<form action="sendContactForm" method="post"  class="sends-email ctc-form" >
			<div class="top-row">
				<label><span class="ctc-hide">Name</span>
					<input type="text" name="name" placeholder="Name:">
				</label>
				<label><span class="ctc-hide">Phone</span>
					<input type="text" name="phone" placeholder="Phone:">
				</label>
				<label><span class="ctc-hide">Email</span>
					<input type="text" name="email" placeholder="Email:">
				</label>
			</div>
			<label><span class="ctc-hide">Message / Questions:</span>
				<textarea name="message" cols="30" rows="10" placeholder="Message / Questions:"></textarea>
			</label>
			<!-- <label for="g-recaptcha-response"><span class="ctc-hide">Recaptcha</span></label> -->
			<div class="bot-row">
				<div class="checkboxes">
					<label>
						<input type="checkbox" name="consent" class="consentBox">I hereby consent to having this website store my submitted information so that they can respond to my inquiry.
					</label><br>
					<?php if( $this->siteInfo['policy_link'] ): ?>
					<label>
						<input type="checkbox" name="termsConditions" class="termsBox"/>I hereby confirm that I have read and understood this website's <a href="<?php $this->info("policy_link"); ?>" target="_blank">Privacy Policy.</a>
					</label>
				</div>
				<div class="g-recaptcha"></div>
			</div>
			<?php endif ?>
			<button type="submit" class="ctcBtn" disabled>SUBMIT FORM</button>
		</form>
	</div>
</div>
