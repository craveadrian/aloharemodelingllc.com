<div id="content">
	<div class="row">
		<h1>About Us</h1>
		<img src="public/images/content/about-page-img1.jpg" alt="Man in construction suit" class="about-page-image1">
		<p class="about-paragraph">It has been over many years that Aloha Remodeling & Handiman Services, LLC. being a family business is privileged to offer its services as one of the best Hawaii General Construction, Building Restoration, Carpentry, Design, Home Improvement, Building Cleaning as well Home addition and maintains a highly valued clientele. </p>
		<p class="about-paragraph">Our Licensed and Fully Insured Company provides Free of Cost Inspection, Free Estimation on the site, complete guidance regarding your General Construction needs, and on top of all “Satisfaction”. Realizing the fact that the maintenance of your Houses, Commercial Buildings, Warehouses and Offices etc we are available as one of the most successful and reliable General Construction Contractors in Honolulu, HI.</p>
		<h3 class="serviceAreaHeader">Service Area</h3>
		<ul class="serviceArea">
			<li>Honolulu</li>
			<li>Kailua</li>
			<li>Tripler Army Medical Center</li>
			<li>Waimanalo</li>
			<li>Camp H M Smith</li>
			<li>Aiea</li>
			<li>Fort Shafter</li>
		</ul>
	</div>
</div>
